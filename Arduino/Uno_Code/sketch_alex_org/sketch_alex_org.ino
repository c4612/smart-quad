const int analogPin = A0; 
const int ledCount = 10;
int potmeterVal = 0; 
int ledPins[] = {4, 5, 6, 7, 8, 9, 10, 11, 12, 13}; 

void setup() {
   for (int thisLed = 0; thisLed < ledCount; thisLed++) {
      pinMode(ledPins[thisLed], OUTPUT);

      Serial.begin(9600);
   }
}

void loop() {
   int sensorReading = analogRead(analogPin);
   int ledLevel = map(sensorReading, 0, 1023, 0, ledCount);
   for (int thisLed = 0; thisLed < ledCount; thisLed++) {
      if (thisLed < ledLevel) {
         digitalWrite(ledPins[thisLed], LOW);
      }else { 
         digitalWrite(ledPins[thisLed], HIGH);


      }
   }

   potmeterVal = analogRead(analogPin);
   
   Serial.println(potmeterVal -824);
   
   delay(100);
}
