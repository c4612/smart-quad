from get_h2consumption_from_power import get_h2_consumption_from_power
from get_voltage_from_current import get_voltage_from_current
from get_current_from_load import get_current_from_load

def calculate_air_usage(load_power, average_cell_voltage):
    air_usage = 3.57e-7 * (load_power / average_cell_voltage)  # [kg/s]
    return air_usage


def calculate_oxigen_usage(load_power, average_cell_voltage):
    oxigen_usage = 8.29e-5 * (load_power / average_cell_voltage)  # [g/s]
    return oxigen_usage


def calculate_hydrogen_usage(load_power, average_cell_voltage):
    hydrogen_usage = 1.05e-5 * (load_power / average_cell_voltage)  # [g/s]
    return hydrogen_usage


def calculate_water_production(load_power, average_cell_voltage):
    water_production = 9.34e-5 * (load_power / average_cell_voltage)  # [g/s]
    return water_production


def calculate_heat_generation(load_power, average_cell_voltage):
    heat_generation = load_power * ((1.25/average_cell_voltage) - 1)  # [W]
    return heat_generation

import matplotlib.pyplot as plt

TANK_VOLUME = 1000
CURRENT_WE_ASK = 10  # [A]
CELLS_IN_STACK = 48
LOAD_POWER = get_voltage_from_current(CURRENT_WE_ASK)*CURRENT_WE_ASK
AVERAGE_CELL_VOLTAGE = (LOAD_POWER / CURRENT_WE_ASK) / CELLS_IN_STACK
print('average cell voltage: {}'.format(AVERAGE_CELL_VOLTAGE))
print('total stack voltage: {}'.format(AVERAGE_CELL_VOLTAGE*CELLS_IN_STACK))

simulation = False
if simulation:
    time_in_seconds = 0
    while TANK_VOLUME > 0:
        time_in_seconds += 1
        TANK_VOLUME -= calculate_hydrogen_usage(LOAD_POWER, AVERAGE_CELL_VOLTAGE)
        # print('tank volume: {}'.format(TANK_VOLUME))
        if time_in_seconds % 60 == 0:
            pass
            # print('{} minutes past'.format(time_in_seconds//60))
    print()
    print('{}:{} minutes past'.format(time_in_seconds//60, time_in_seconds%60))
    print('we have generated {} kWh'.format(time_in_seconds*LOAD_POWER/3600000))
    print()

load_power = []
h2_usage = []
efficiency = []
current = []
stack_voltages = [0]
hydrogen_usage = [0]

'''
for i in range(1, 40):
    efficiency.append(get_voltage_from_current(i)/(CELLS_IN_STACK*1.25))
    load_power.append(get_voltage_from_current(i)*i)
    h2_usage.append(get_h2_consumption_from_power(load_power[-1]))
    stack_voltages.append((load_power[-1] / i))'''

for i in range(1000):
    current.append(get_current_from_load(i))
    efficiency.append(get_voltage_from_current(current[-1])/(CELLS_IN_STACK*1.25))
efficiency.append(0)
plt.plot(efficiency)

# plt.scatter(load_power, efficiency)
plt.show()


print('oxigen usage: {} g/s'.format(calculate_oxigen_usage(LOAD_POWER, AVERAGE_CELL_VOLTAGE)))
print('hydrogen usage: {} g/s'.format(calculate_hydrogen_usage(LOAD_POWER, AVERAGE_CELL_VOLTAGE)))
print('water production: {} g/s'.format(calculate_water_production(LOAD_POWER, AVERAGE_CELL_VOLTAGE)))
print('generated heat: {} W'.format(calculate_heat_generation(LOAD_POWER, AVERAGE_CELL_VOLTAGE)))