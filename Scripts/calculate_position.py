from math import pi, sin, cos

"""
above straight:     0 - 1093 px
upper left corner:  1093 - 1407
left straight:      1407 - 1780
lower left corner:  1780 - 2094

lower straight:     2094 - 3187
lower right corner: 3187 - 3501
right straing:      3501 - 3874
upper right corner: 3874 - 4188
4188 / 12 px/m = 349m round
"""


def calculate_pixels_from_distance(distance, last_coordinate):
    pixel = (distance*12) % 4188
    #print(distance, pixel)
    if 0 <= pixel < 1093:
        relative_pixel = pixel
        coordinate = [1361-relative_pixel, 43]
        in_corner = False
    elif 1093 <= pixel < 1407:
        relative_pixel = pixel - 1093
        angle = relative_pixel/628 * pi
        x_centre, y_centre = 268, 243
        x_circle, y_circle = x_centre - 200*sin(angle), y_centre - 200*cos(angle)
        coordinate = [x_circle, y_circle]
        in_corner = True

    elif 1407 <= pixel < 1780:
        relative_pixel = pixel - 1407
        coordinate = [68, 243+relative_pixel]
        in_corner = False
    elif 1780 <= pixel < 2094:
        relative_pixel = pixel - 1780
        angle = relative_pixel/628 * pi
        x_centre, y_centre = 268, 616
        x_circle, y_circle = x_centre - 200*cos(angle), y_centre + 200*sin(angle)
        coordinate = [x_circle, y_circle]
        in_corner = True

    elif 2094 <= pixel < 3187:
        relative_pixel = pixel - 2094
        coordinate = [268+relative_pixel, 816]
        in_corner = False
    elif 3187 <= pixel < 3501:
        relative_pixel = pixel - 3187
        angle = relative_pixel/628 * pi
        x_centre, y_centre = 1361, 616
        x_circle, y_circle = x_centre + 200*sin(angle), y_centre + 200*cos(angle)
        coordinate = [x_circle, y_circle]
        in_corner = True

    elif 3501 <= pixel < 3874:
        relative_pixel = pixel - 3501
        coordinate = [1561, 616-relative_pixel]
        in_corner = False
    else:
        relative_pixel = pixel - 3874
        angle = relative_pixel/628 * pi
        x_centre, y_centre = 1361, 243
        x_circle, y_circle = x_centre + 200*cos(angle), y_centre - 200*sin(angle)
        coordinate = [x_circle, y_circle]
        in_corner = True

    x_speed = coordinate[0] - last_coordinate[0]
    y_speed = coordinate[1] - last_coordinate[1]

    return coordinate, x_speed, y_speed, in_corner
