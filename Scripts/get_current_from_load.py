from get_voltage_from_current import get_voltage_from_current


def get_current_from_load(load, max_current=40):
    current_to_test = max_current/2
    diff = max_current/4
    while diff > 0.001:
        if get_voltage_from_current(current_to_test)*current_to_test > load:
            current_to_test -= diff
        else:
            current_to_test += diff
        diff /= 2
    return current_to_test


unit_test = False
if unit_test:
    print(get_current_from_load(850))
