from numpy import poly1d


def get_h2_consumption_from_power(power):
    if power < 0:
        raise ValueError('negative power is impossible')
    elif power > 1010:
        raise ValueError('this cell is not rated for values above 1010 watt')
    else:
        polynomial_fit = poly1d([0.00000000000006940161, -0.00000000018974027669,
                                 0.00000020586025996361, -0.00010858835190674700,
                                 0.02907804257120010000, 6.59216656209900000000,
                                 16.77432337030760000000])
        h2_consumption = polynomial_fit(power)
    return h2_consumption


check_graph = False
if check_graph:
    import matplotlib.pyplot as plt
    voltages = []
    for i in range(1000):
        voltages.append(get_h2_consumption_from_power(i))
    plt.plot(voltages)
    plt.show()
