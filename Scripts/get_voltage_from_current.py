from numpy import poly1d


def get_voltage_from_current(current):
    if current < 0:
        raise ValueError('negative current impossible')
    elif current > 40:
        raise ValueError('you cannot draw more than 40 Ampères from the cell')
    else:
        polynomial_fit = poly1d([7.101e-8, -1.1523e-5, 0.000703495, -0.0209237, 0.3136098, -2.42113, 45.389])
        voltage = polynomial_fit(current)
    return voltage


check_graph = False
if check_graph:
    import matplotlib.pyplot as plt
    voltages = []
    for i in range(41):
        voltages.append(get_voltage_from_current(i))
    voltages.append(0)
    plt.plot(voltages)
    plt.show()
