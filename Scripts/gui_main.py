import sys

# root = "C:/Users/Administrator/Desktop/CIVON_hydrogen/"
root = "C:/Civon/Smart Quad/smart-quad-main/repos/Scripts/"
# libs = "venv/Lib/site-packages"
try:
    sys.path.append("C:/Users/Gebruiker/Desktop/CIVON_hydrogen/venv/Lib/site-packages")
    # print(sys.path)
except:
    pass

import time
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMainWindow, QApplication, QFrame, QPushButton, QDial, QLabel, QWidget, QVBoxLayout,\
    QGridLayout, QSlider, QComboBox, QFileDialog, QMessageBox
from PyQt5.uic import loadUi
from PyQt5.QtCore import Qt, QThread, pyqtSignal, QTimer
from PyQt5.QtGui import QWheelEvent, QKeyEvent
import keyboard
from pandas import DataFrame, ExcelWriter
from openpyxl import load_workbook, worksheet
from datetime import datetime
from random import randrange

import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

from get_h2consumption_from_power import get_h2_consumption_from_power
from get_voltage_from_current import get_voltage_from_current
from get_current_from_load import get_current_from_load
from calculate_position import calculate_pixels_from_distance

tank_level = 100
current = 0
voltage = 0
efficiency = 0
volumeflow = 0
massflow = 0
plotcounter = 0
max_speed = 0
temperature = 20

quad_mass = 300  # [kg]
static_drag = 90  # [N]
v_quad = 0  # [m/s]
a_quad = 0  # [m/s2]
power = 0  # [N]
race_distance = 500  # [m]
crash_counter = 0

time_list = [0]
tank_level_list = [tank_level]
current_list = [0]
voltage_list = [0]
power_list = [0]
efficiency_list = [0]
volumeflow_list = [0]
temperature_list = [temperature]
a_list = [0]
v_list = [0]
x_list = [0]
temperature_oke_list = [True]
brake_list = [False]
quad_position = 0

dt = 0.1
cells_in_stack = 48
temperature_mass = 4
platinum_specific_heat = 610  # [J/kgK]
outside_temperature = 15
temperature_shut_off_temperature = 65
temperature_maximum_start_temperature = 45

temperature_cooling_capacity = 0.05  # [K/dTs]
temperature_multiplier = 10

attempt_time = 0
race_active = False
results_pages = []

fast_empty = False
show_corner = False
show_temperature = True
temperature_oke_flag = True

import socket
import re

serverMACaddress = '98:d3:31:fb:13:60'
port = 1
size = 1024

try:
    serverObj = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)
    serverObj.connect((serverMACaddress, port))
    serverConnected = True
    # pause for 2 seconds, will this prevent initial start-up crash?
    print('BT connection in progress...')
    time.sleep(2)
    print('Continuing program.')
    
except:
    print('Failed BT connection, no connection to quad established.')
    serverConnected = False

class ResultsPage(QMainWindow):
    global max_speed, quad_position, quad_position, attempt_time, v_list, current_list, efficiency_list
    global voltage_list, power_list

    def __init__(self):
        super(ResultsPage, self).__init__()
        loadUi('resultForm.ui', self)

        if quad_position >= race_distance:
            self.label = self.findChild(QLabel, 'Title')
            self.label.setText('GEHAALD IN {:.1f} SECONDEN!'.format(attempt_time))

        self.label = self.findChild(QLabel, 'FinalDistance')
        self.label.setText('{:.1f} m'.format(quad_position))

        self.label = self.findChild(QLabel, 'FinalTime')
        self.label.setText('{:.1f} sec'.format(attempt_time))

        self.label = self.findChild(QLabel, 'MaxSpeed')
        self.label.setText('{:.1f} km/u'.format(max_speed*3.6))

        self.label = self.findChild(QLabel, 'AvgSpeed')
        self.label.setText('{:.1f} km/u'.format(sum(v_list)/len(v_list)))

        self.label = self.findChild(QLabel, 'AvgCurrent')
        self.label.setText('{:.1f} A'.format(sum(current_list)/len(current_list)))

        self.label = self.findChild(QLabel, 'AvgEfficiency')
        self.label.setText('{:.1f} %'.format(sum(efficiency_list)/len(efficiency_list)))

        self.label = self.findChild(QLabel, 'AvgPower')
        self.label.setText('{:.1f} watt'.format(sum(power_list)/len(power_list)))

        self.label = self.findChild(QLabel, 'AvgVoltage')
        print(self.label)
        self.label.setText('{:.1f} V'.format(sum(voltage_list)/len(voltage_list)))

        print('init the button')
        self.SaveExcelButton = self.findChild(QPushButton, 'DownloadData')
        print(self.SaveExcelButton)
        print('connect function to the button')
        self.SaveExcelButton.clicked.connect(lambda: self.create_excel_file())
        print('show the frame')
        self.show()

    def create_excel_file(self):
        print('getting the globals')
        global time_list, tank_level_list, current_list, voltage_list, power_list, efficiency_list, volumeflow_list
        global temperature_list, a_list, v_list, x_list, temperature_oke_list, brake_list

        print('open the filedialog')
        file = QFileDialog.getExistingDirectory(self, "Select Directory")
        print(file)

        # create a pandas DataFrame, because that is easy to write to an excel file
        date_time_string = datetime.now().strftime("%m-%d-%Y_%H-%M-%S")
        print(date_time_string)

        all_lists = [time_list, tank_level_list, current_list, voltage_list, power_list, efficiency_list,
                     volumeflow_list, temperature_list, a_list, v_list, x_list, temperature_oke_list, brake_list]
        max_list_length = 0
        for race_list in all_lists:
            if len(race_list) > max_list_length:
                max_list_length = len(race_list)
        print('max_list_length: {}'.format(max_list_length))

        # make the list equal length:
        for race_list in all_lists:
            if len(race_list) < max_list_length:
                for i in range(max_list_length - len(race_list)):
                    race_list.insert(0, 0)

        def add_noise(data, amplitude_ratio):
            # JB** TypeError: 'list' object cannot be interpreted as an integer -> data = [data[i] + randrange(-amplitude_ratio*data[i], amplitude_ratio*data[i]) for i in range(data)]
            return data

        all_race_data = DataFrame({'Tijd [s]': time_list,
                                   'Waterstof Tank Niveau [%]': tank_level_list,
                                   'Gevraagde Stroom [A]': current_list,
                                   'Resulterende Spanning [V]': add_noise(voltage_list, 0.05),
                                   'Brandstofcel Vermogen [W]': add_noise(power_list, 0.05),
                                   'Brandstofcel Efficientie [%]': add_noise(efficiency_list, 0.05),
                                   'Waterstof Volumestroom [ml/s]': add_noise(volumeflow_list, 0.1),
                                   'Brandstofcel Temperatuur [Celsius]': add_noise(temperature_list, 0.05),
                                   'Brandstofcel actief': temperature_oke_list,
                                   'Quad Positie [m]': x_list,
                                   'Quad Snelheid [m/s]': add_noise(v_list, 0.05),
                                   'Quad Versnelling [m/s^2]': add_noise(a_list, 0.05),
                                   'Quad Rem': brake_list})

        writer = ExcelWriter('{}/{}_quad_race_resultaten.xlsx'.format(file, date_time_string), engine='openpyxl')
        all_race_data.to_excel(writer, sheet_name="Sheet1", startrow=0, startcol=0, index=True, header=True)
        writer.save()
        QMessageBox.about(self, "", "Saved the data succesfully")


def quad(power, brake):
    global v_quad, a_quad, static_drag, quad_mass, quad_position, dt, a_list, v_list, x_list
    global position, max_speed, attempt_time, race_active, results_pages, temperature

    acc_force = power / 2  # [N]
    brake_force = 1000 if brake else 0

    a_quad = (acc_force - brake_force - 2.5 * v_quad ** 2 - static_drag) / quad_mass
    v_quad += a_quad * dt
    if v_quad < 0:
        v_quad = 0
    elif v_quad > max_speed:
        max_speed = v_quad
    quad_position += v_quad * dt

    # print('force: {:.2f}N | d_drag: {:.2f}N | acc: {:.2f}m/s2 | speed: {:.2f}m/s | temperature: {:.2f}'
    #       .format(acc_force, 3 * v_quad ** 2, a_quad, v_quad, temperature))


class HydrogenCellThread(QThread):

    any_signal = pyqtSignal(int)
    finish_signal = pyqtSignal(int)

    def __init__(self, parent=None, index=0):
        super(HydrogenCellThread, self).__init__(parent)
        self.index = index
        self.is_running = True

    def run(self):
        # print('Starting thread...', self.index)
        global tank_level, current, voltage, efficiency, volumeflow, massflow, dt, quad_mass, v_quad, a_quad, fast_empty
        global attempt_time, race_active, a_list, v_list, x_list, quad_position, temperature, temperature_list
        global max_speed, temperature_oke_list, brake_list, crash_counter
        global time_list, tank_level_list, current_list, voltage_list, power_list, efficiency_list, volumeflow_list
        global temperature_oke_flag, temperature_shut_off_temperature, temperature_maximum_start_temperature
        global serverConnected, serverObj
        
        dataOver = b''
                    
        while True:
            
            if serverConnected:
                try:
                    dataNew = serverObj.recv(size)
                    data = dataOver + dataNew
                    dataOver = data
                    if data:
                        if data[-1] == 10:
                            allNumber = re.findall(r'(-{0,1}\d+)', data.decode('ASCII'))
                            if len(allNumber) > 0:
                                #print(float(allNumber[-1]))
                                rawDialValue = float(allNumber[-1])
                                # succesfully read a new setpoint by Bluetooth, set to dial
                                dialValue = int((rawDialValue - 200.0)/-1000.0*80.0)
                                current = dialValue/2
                                #print(dialValue)
                                
                            dataOver = b''
                            
                            
                except:
                    print('Closing socket')
                    serverObj.close()
                    serverConnected = False
                
                # safety
                if current < 0:
                    current = 0
                elif current > 40:
                    current = 40
                    
            if tank_level > 0:
                voltage = get_voltage_from_current(current)
            else:
                tank_level = 0
                voltage = 0

            if not race_active and v_quad > 0:
                quad_position = 0
                attempt_time = 0
                max_speed = 0
                crash_counter = 0
                temperature = 20

                time_list = [0]
                tank_level_list = [tank_level]
                current_list = [0]
                voltage_list = [0]
                power_list = [0]
                efficiency_list = [0]
                volumeflow_list = [0]
                temperature_list = [temperature]
                a_list = [0]
                v_list = [0]
                x_list = [0]
                temperature_oke_list = [True]
                brake_list = [False]

                race_active = True

            if race_active:
                attempt_time += dt

            if temperature > temperature_shut_off_temperature:
                temperature_oke_flag = False
            if not temperature_oke_flag and temperature < temperature_maximum_start_temperature:
                temperature_oke_flag = True

            power = current*voltage if temperature_oke_flag else 0
            volumeflow = get_h2_consumption_from_power(power)/60*dt  # [ml/s]
            massflow = volumeflow*8.2e-5  # [g/s]
            generated_heat = ((cells_in_stack*1.25)-voltage)*current if power else 0

            temperature += temperature_multiplier * generated_heat * dt / (platinum_specific_heat * temperature_mass)
            temperature -= temperature_cooling_capacity * dt * (temperature - outside_temperature)

            brake = keyboard.is_pressed('space')
            quad(power, brake)
            if fast_empty:
                tank_level -= massflow*10000
            else:
                tank_level -= massflow*100  # 400

            if race_active and ((tank_level <= 0 and v_quad <= 0) or
                                quad_position >= race_distance or
                                crash_counter > 1/dt):
                # print(1)
                race_active = False
                v_quad = 0
                a_quad = 0

                self.finish_signal.emit(1)
                # print(3)
                # reset race

            if tank_level < 0:
                pass
            time.sleep(dt)
            self.any_signal.emit(tank_level)

            if race_active:
                time_list.append(time_list[-1]+dt)
                tank_level_list.append(tank_level)
                current_list.append(current)
                voltage_list.append(voltage)
                power_list.append(current * voltage)
                efficiency_list.append(voltage/(cells_in_stack*1.25)*100)
                temperature_list.append(temperature)
                volumeflow_list.append(volumeflow)
                temperature_oke_list.append(temperature_oke_flag)
                brake_list.append(brake)
                a_list.append(a_quad)
                v_list.append(v_quad)
                x_list.append(quad_position)

    def stop(self):
        self.is_running = False
        # print('stopping thread...', self.index)
        self.terminate()


class MplCanvas(FigureCanvas):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        # print(self.fig)
        self.axes = self.fig.add_subplot(111)
        # print(self.axes)
        super(MplCanvas, self).__init__(self.fig)
        self.fig.tight_layout()


class SplashScreen(QMainWindow):
    def __init__(self):
        super(SplashScreen, self).__init__()
        loadUi('CIVONsimulation.ui', self)

        tank_base = self.centralWidget().findChild(QFrame, 'FuelTank')
        self.progressbar = tank_base.findChild(QFrame, 'TankLevel')

        self.speedometer = self.centralWidget().findChild(QFrame, 'Speedometer')
        self.speed_display = self.speedometer.findChild(QLabel, 'SpeedDisplay')

        self.tank_level_label = self.centralWidget().findChild(QLabel, 'TankLevelLabel')
        self.efficiency_label = self.centralWidget().findChild(QLabel, 'EfficiencyLabel')
        self.voltage_label = self.centralWidget().findChild(QLabel, 'VoltageLabel')
        self.power_label = self.centralWidget().findChild(QLabel, 'PowerLabel')
        self.h2_volume_flow_label = self.centralWidget().findChild(QLabel, 'H2VolumeFlow')
        self.h2_mass_flow_label = self.centralWidget().findChild(QLabel, 'H2MassFlow')
        self.temperature_label = self.centralWidget().findChild(QLabel, 'TemperatureLabel')

        self.quad_distance_label = self.centralWidget().findChild(QLabel, 'Distance')
        self.attempt_time_label = self.centralWidget().findChild(QLabel, 'AttemptTime')

        load_cell_boarder = self.centralWidget().findChild(QFrame, 'LoadCell')
        load_cell = load_cell_boarder.findChild(QFrame, 'LoadCell2')
        current_display = load_cell.findChild(QFrame, 'DisplayBorder')
        self.current_label = current_display.findChild(QLabel, 'CurrentDisplay')

        self.load_dial = load_cell.findChild(QDial, 'CurrentDial')
        self.load_dial.setMinimum(0)
        self.load_dial.setMaximum(80)
        self.load_dial.setValue(0)
        self.load_dial.valueChanged.connect(self.slider_moved)

        self.fuel_cell = self.centralWidget().findChild(QWidget, 'FuelCell')
        self.cell_plates = self.fuel_cell.findChild(QFrame, 'CellPlates')

        self.ButtonStart = self.centralWidget().findChild(QPushButton, 'ButtonStart')
        self.ButtonStart.clicked.connect(lambda: self.start_thread())

        self.ButtonStop = self.centralWidget().findChild(QPushButton, 'ButtonStop')
        self.ButtonStop.clicked.connect(lambda: self.stop_thread())
        self.ButtonStop.setEnabled(False)

        self.ButtonBrake = self.centralWidget().findChild(QPushButton, 'Brake')
        # ButtonBrake.isChecked().connect(lambda: self.brake()) #  .connect()

        self.RaceTypeSelection = self.centralWidget().findChild(QComboBox, 'RaceType')
        self.RaceTypeSelection.clear()
        self.RaceTypeSelection.addItem('Drag Race (rechte stuk, 95m) met 10% H2')
        self.RaceTypeSelection.addItem('Sprint Race (1 ronde, 445m)')
        self.RaceTypeSelection.addItem('Endurance Race (totaan lege tank)')

        self.quad = self.centralWidget().findChild(QFrame, 'Quad')
        self.quad_coordinates = [1361, 43]
        self.quad.move(self.quad_coordinates[0]-8, self.quad_coordinates[1]-8)

        self.x_speed = 0
        self.y_speed = 0
        self.in_corner = False
        self.last_coordinates = [0, 0]

        self.canvas = MplCanvas(self, width=5, height=4, dpi=100)
        self.layout = self.centralWidget().findChild(QGridLayout, 'gridLayout')
        self.layout.addWidget(self.canvas, 1, 1, 1, 1)
        self.canvas.axes.plot([])

        self.show()

    def move_quad(self, position):
        global show_corner
        if show_corner:
            if position > 0.2:
                self.quad.move(int(1280-600*(position-0.2)), int(650-600*0.2))
            else:
                self.quad.move(int(1280), int(650-600*position))
        else:
            self.quad.move(int(1280), int(650 - 600 * position))

    def move_quad_slider(self, too_fast=False):
        distance = self.race_slider.value()
        if not too_fast:
            self.quad_coordinates, self.x_speed, self.y_speed = \
                calculate_pixels_from_distance(distance, self.last_coordinates)
        else:
            self.quad_coordinates = [self.last_coordinates[0] + self.x_speed,
                                     self.last_coordinates[1] + self.y_speed]
        self.last_coordinates = self.quad_coordinates

    def move_quad_circuit(self, distance, too_fast=False):
        global crash_counter
        if too_fast and self.in_corner:
            self.quad_coordinates = [self.last_coordinates[0] + self.x_speed,
                                     self.last_coordinates[1] + self.y_speed]
            crash_counter += 1
        else:
            self.quad_coordinates, self.x_speed,\
                self.y_speed, self.in_corner = calculate_pixels_from_distance(distance, self.last_coordinates)

        self.last_coordinates = self.quad_coordinates
        self.quad.move(int(self.quad_coordinates[0]-8), int(self.quad_coordinates[1]-8))

    def slider_moved(self):
        global current
        current = self.load_dial.value()/2
        self.update_labels_and_plot()

    def wheelEvent(self, event: QWheelEvent):
        self.load_dial.wheelEvent(event)

    def brake(self):
        print('brake')
        global v_quad
        print(Qt.Key_Space)
        if self.ButtonBrake.isChecked():
            v_quad -= dt * 10

    def start_thread(self):

        self.ButtonStop.setEnabled(True)
        self.ButtonStart.setEnabled(False)

        global race_distance, tank_level
        racetype = self.RaceTypeSelection.currentIndex()
        if racetype == 0:
            race_distance = 95
            tank_level = 10
        if racetype == 1:
            race_distance = 445
            tank_level = 100
        if racetype == 2:
            race_distance = 100000
            tank_level = 100

        self.thread = HydrogenCellThread(parent=None, index=1)
        self.thread.finish_signal.connect(lambda: self.stop_thread())
        self.thread.start()
        self.thread.any_signal.connect(self.update_labels_and_plot)

    def stop_thread(self):
        print('entered stop_threar()')
        self.ButtonStop.setEnabled(False)
        self.ButtonStart.setEnabled(True)
        self.thread.stop()
        print('add a new results page')
        results_pages.append(ResultsPage())
        # print(results_pages)
        print('show a new results page')
        results_pages[-1].show()

    def button_reset(self):
        # print('button up')
        global tank_level, temperature, race_active, race_distance
        temperature = 20
        race_active = False
        race_distance = 0
        self.update_labels_and_plot()

    def update_labels_and_plot(self):
        # PROGRESSBAR STYLESHEET BASE
        global tank_level, current, voltage, efficiency, volumeflow, massflow, cells_in_stack
        global tank_level_list, current_list, voltage_list, efficiency_list, volumeflow_list, time_list
        global plotcounter, temperature_list, temperature_oke_flag
        global quad_position, v_quad, attempt_time, temperature

        plotcounter += 1
        if plotcounter % 5 == 0:
            self.canvas.axes.clear()
            self.canvas.axes.plot(tank_level_list)
            self.canvas.axes.plot(v_list)
            self.canvas.axes.plot(x_list)
            self.canvas.axes.plot(temperature_list)
            self.canvas.draw()

        if v_quad > 10:
            self.move_quad_circuit(quad_position, too_fast=True)
        else:
            self.move_quad_circuit(quad_position, too_fast=False)

        if race_distance < 10000:
            self.quad_distance_label.setText('{:.1f} / {}m'.format(quad_position, race_distance))
        else:
            self.quad_distance_label.setText('{:.1f} m'.format(quad_position))

        self.attempt_time_label.setText('{:.1f} s'.format(attempt_time))
        self.tank_level_label.setText('Waterstof Niveau: {:.1f}%'.format(tank_level))
        self.efficiency_label.setText('Efficientie: {:.1f}%'.format(efficiency))
        self.voltage_label.setText('{:.1f} Volt'.format(voltage))
        self.current_label.setText(' {:.1f} A'.format(current))
        self.power_label.setText('{:.1f} Watt'.format(voltage*current))
        self.h2_mass_flow_label.setText('H2 Stroom: {:.5f} g/s'.format(massflow))
        self.h2_volume_flow_label.setText('H2 Stroom: {:.2f} mL/s'.format(volumeflow))
        self.efficiency_label.setText('Efficientie: {:.2f}%'.format(voltage/(cells_in_stack*1.25)*100))
        self.temperature_label.setText('Temperatuur: {:.2f} Graden'.format(temperature))
        self.speed_display.setText('{:.1f} Km/u'.format(v_quad*3.6))

        # update dial in case value came from BT
        self.load_dial.setValue(int(current*2.0))
                                

        styleSheet = """
        QFrame{
        background-color: qlineargradient(spread:pad, x1:1, y1:1, x2:1, y2:0, stop:{STOP_1} rgba(0, 0, 255, 255), stop:{STOP_2} rgba(0, 255, 255, 255));
        }
        """
        newStylesheet = styleSheet.replace("{STOP_1}",
                                           str(max(0, (tank_level/100)-0.001))).replace("{STOP_2}",
                                                                                  str(max(0.001, (tank_level/100))))
        self.progressbar.setStyleSheet(newStylesheet)

        styleSheet = """
        QFrame{
        background-color: qconicalgradient(cx:0.5, cy:0.5, angle:{angle}, stop:0.987 rgba(0, 0, 0, 0), stop:0.988 rgba(219, 13, 31, 255));
        }
        """
        newStylesheet = styleSheet.replace("{angle}", str(180-(3.6*v_quad)*3))
        self.speedometer.setStyleSheet(newStylesheet)

        if temperature_oke_flag:
            styleSheet = 'QFrame{background-color: rgb(150, 150, 150);}'
            self.cell_plates.setStyleSheet(styleSheet)
        else:
            if int(attempt_time*4) % 2 == 0:
                styleSheet = 'QFrame{background-color: rgb(255, 0, 0);}'
            else:
                styleSheet = 'QFrame{background-color: rgb(255, 150, 150);}'
            self.cell_plates.setStyleSheet(styleSheet)

app = QApplication(sys.argv)
mainwindow = SplashScreen()
app.exec_()
