# script to test bluetooth connection to a server using sockets
import socket
import re

serverMACaddress = '98:d3:31:fb:13:60'
port = 1
size = 1024

s = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)

s.connect((serverMACaddress, port))

try:
    dataOver = b''
    while 1:
        dataNew = s.recv(size)
        data = dataOver + dataNew
        dataOver = data
        if data:
            if data[-1] == 10:
                allNumber = re.findall(r'(-{0,1}\d+)', data.decode('ASCII'))
                print(float(allNumber[-1]))
                dataOver = b''
                
except:
    print('Closing socket')
    s.close()