import pygame
import pygame.freetype
pygame.init()

# setup the drawing window
screen = pygame.display.set_mode([500, 500])
screen.fill((255, 255, 255))

pygame.font.init()
font = pygame.font.Font('freesansbold.ttf', 12)
right_click_mouse_speed = 5

ball_speed = 0
ball_acc = 0.1
ball_pos = (0, 0)
ball_falling = False
in_the_field = False

# run until the user asks to quit
running = True
while running:
    # did the user click the window close button?
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            ball_falling = True
            ball_speed = 0
            in_the_field = True
            ball_pos = pygame.mouse.get_pos()
            print(ball_pos)
            screen.fill((255, 255, 255))
            pygame.draw.circle(screen, (0, 0, 255), ball_pos, 50)
            pygame.display.flip()
        if event.type == pygame.MOUSEBUTTONUP and event.button == 3:
            ball_speed = -right_click_mouse_speed
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 4:
            right_click_mouse_speed += 1
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 5:
            right_click_mouse_speed -= 1

    if ball_falling:
        ball_speed += ball_acc
        ball_pos = (ball_pos[0], ball_pos[1]+ball_speed)

        screen.fill((255, 255, 255))
        pygame.draw.circle(screen, (0, 0, 255), ball_pos, 50)
        pygame.display.flip()

    if ball_pos[1] <= 50 and in_the_field:
        in_the_field = False
        ball_speed = -0.8 * ball_speed
    if ball_pos[1] >= 450 and in_the_field:
        in_the_field = False
        ball_speed = -0.8 * ball_speed

    if 50 < ball_pos[1] < 450:
        in_the_field = True

    string = 'speed if clicking: {} | current speed: {} | current pos: {}'.format(right_click_mouse_speed,
                                                                                  int(ball_speed),
                                                                                  int(ball_pos[1]))

    text = font.render(string, True, (0, 0, 0), (200, 200, 200))
    textRect = text.get_rect()
    textRect.center = (250, 10)
    screen.blit(text, textRect)
    pygame.display.update()

    pygame.time.delay(10)

pygame.quit()
